linux-wpan
==========

Mailinglist: <[linux-wpan@vger.kernel.org](mailto:linux-wpan@vger.kernel.org)\> [Mailinglist-Archive](http://www.spinics.net/lists/linux-wpan)

IRC: #linux-wpan on irc.freenode.net

wpan-tools
----------

To access the nl802154 netlink inteface and also for checking the network connectivity you will need the wpan-tools.

Dependencies:

*   netlink library [libnl](http://www.infradead.org/~tgr/libnl/).


These tools contains:

**iwpan** based on the wireless [iw](http://wireless.kernel.org/en/users/Documentation/iw) tool.

**wpan-ping** Ping utility on IEEE 802.15.4 level.

### Download

For the last release check out [releases page](https://github.com/linux-wpan/wpan-tools/releases)
